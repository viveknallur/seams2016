const Trait = require("./traits.js").Trait;
const cop = require("./context-traits.js");

const SIZE = 5; //80
const GROUPS = 5; //10
var cells = new Array();
var threeDContexts = new Array();
var bigAreaContexts = new Array();

function createBoard() {
	for(var i=0; i<SIZE; i++) {
		for(var j=0; j<SIZE; j++) {
			cells[cells.length] = Cell(i, j);
		}
	}
	threeDAdaptations(cells);
};

function print() {
	var b = new Array(SIZE);
	for(var i=0; i<SIZE; i++) {
		b[i] = new Array(SIZE);
		for(var j=0; j<SIZE; j++) {
			b[i][j] = 0;
		}
	}
	for(var i=0; i<cells.length; i++) {
		b[cells[i].x][cells[i].y] = cells[i].state;
	}
	console.log(b);
}


function step() {
	var cellsNext = new Array();
	console.log("step");		
	
	for(var i=0; i<cells.length; i++) {
		cellsNext[cellsNext.length] = cells[i].step();
	}
	cells = cellsNext;
	cellsNext = null;
	threeDAdaptations(cells);
	print();
}

function init(aliveAtStart) {
	for(var c=0; c<cells.length; c++) {
		for(var i=0; i<aliveAtStart.length;	i++) {
			if(cells[c].x == aliveAtStart[i][0] && cells[c].y == aliveAtStart[i][1]) {
				cells[c].state = 1;
			}
		}
	}
	print();
}


TCell = Trait({
	x: -1,
	y: -1,
	state: 0,
	setCoordinates: function(posX, posY) {
		this.x = posX;
		this.y = posY;
	},
	step: function() {
		var n = this.countAliveNeighbors();
		var c = new Cell(this.x, this.y);
		c.state = this.state;
		return this.conditions(c, n);
	},
	countAliveNeighbors: function() {
		var count = 0;
			for(dx=-1; dx<= 1; dx++){
				for(dy = -1; dy <= 1; dy++) {
					if(dx == 0 && dy == 0) {}
					else {
						for(var i=0; i<cells.length; i++) {
							if(this.x+dx == cells[i].x && this.y+dy == cells[i].y && cells[i].state) {
							count ++;	
							}
						}
					}
				}
			}
		return count;
	},
	conditions: function(cell, alive) {
		switch (alive) {
			case 0:
			case 1:
				cell.state = 0;
				break;
			case 2:
				break; 
			case 3:
				cell.state = 1;
				break;
			default:
				cell.state = 0;
		}
		return cell;
	},
	getDimensions: function() {
		return 2;
	},
});


function Cell(posX, posY) {
	var cell = Object.create(Object.prototype, TCell);
	cell.setCoordinates(posX, posY);
	return cell;
}

//-------------- ADAPTATIONS
Cell3D = Trait({
	z: -1,
	td: false,
	threeDCells: new Array(SIZE),
	setCoordinates: function(posX, posY, posZ) {
		this.x = posX;
		this.y = posY; 
		this.z = posZ;
	},
	getDimensions: function() {
		return 3;
	},
	step: function() {
		var newThreeDCells = new Array(this.threeDCells.length);
		for(var k=0; k<this.threeDCells.length; k++) {
			var n = this.threeDCells[k].countAliveNeighbors();
			var c = new Cell(this.threeDCells[k].x, this.threeDCells[k].y);
			c.setCoordinates(this.threeDCells[k].x, this.threeDCells[k].y, this.threeDCells[k].z);
			c.state = this.threeDCells[k].state;
			 newThreeDCells[k] = this.conditions(c, n); 
		}
		this.state = newThreeDCells[0].state;
		threeDCells = newThreeDCells;
		newThreeDCells = null;
		return this;
	},
	countAliveNeighbors: function() {
		var count = 0;
		for(var dx=-1; dx<= 1; dx++){
			for(var dy=-1; dy<=1; dy++) {
				for(var dz=-1; dz<=1; dz++) {
					if(dx == 0 && dy == 0 && dz == 0) {}
					else {
						for(var i=0; i<cells.length; i++) {
							if(!threeDContexts[i].isActive()) {
								count = cells[i].countAliveNeighbors();
							} else {
								for(var k=0; k<cells[i].threeDCells.length; k++) {
									if(this.x+dx == cells[i].x && this.y+dy == cells[i].y && this.z+dz == cells[i].z && cells[i].threeDCells[k].state) {
										count ++;	
									}
								}
							}
						}
					}
				}
			}
		}
		return count;
	},
	make3D: function() {
		if(!this.td) {
			this.z = 0;
			this.threeDCells[0] = this;
			for(var k=1; k<SIZE; k++) {
				this.threeDCells[k] = Cell(this.x, this.y);
				cells[cells.length-1].setCoordinates(this.x, this.y, k);
			}
			this.td = true;
		}
	},
});


function threeDAdaptations(arr) {
	threeDContexts = new Array(SIZE*SIZE);
	for(var i=0; i<SIZE; i++) {	
		for(var j=0; j<GROUPS; j++) {
			var ctx = new cop.Context({name: "ThreeD"+(i*SIZE + j)});
			threeDContexts[(i*SIZE + j)] = ctx;
			ctx.adapt(arr[(i*SIZE + j)], Cell3D);
		}
	}
}


//ThreeD.adapt(TCell, Cell3D);
//ThreeD.adapt(TGOL, GOL3D);

BigAreaCell = Trait({
	countAliveNeighbors: function() {
		var count = 0;
			for(dx=-2; dx<= 2; dx++){
				for(dy = -2; dy <= 2; dy++) {
					if(dx == 0 && dy == 0) {}
					else {
						for(var i=0; i<cells.length; i++) {
							if(this.x+dx == cells[i].x && this.y+dy == cells[i].y && cells[i].state) {
							count ++;	
							}
						}
					}
				}
			}
		return count;
	},
	conditions: function(cell, alive) {
		switch (alive) {
			case 0:
			case 1:
			case 2:
			case 3:
				cell.state = 0;
				break;
			case 4:
				break; 
			case 6:
				cell.state = 1;
				break;
			default:
				cell.state = 0;
		}
		return cell;
	}
});


function BigAreaAdaptations(arr) {
	bigAreaContexts = new Array(SIZE*SIZE);
	for(var i=0; i<SIZE; i++) {	
		for(var j=0; j<GROUPS; j++) {
			var ctx = new cop.Context({name: "BigArea"+(i*SIZE + j)});
			bigAreaContexts[(i*SIZE + j)] = ctx;
			ctx.adapt(arr[(i*SIZE + j)], BigAreaCell);
		}
	}
}

//BigArea.adapt(TCell, BigAreaCell);



//-------------- SURROUNDING ENVIRONMENT CHANGES
/*
var gol = new GOL();
gol.init([[22,36], [23,37], [24,35], [24,36], [24,37]]);
var sensorReading = 1;
function environmentRead() {
	var newReading = Math.floor(Math.random()*(4-1) + 1);
	console.log(sensorReading + "  -  " + newReading);
	if(sensorReading == 1 && newReading == 2) {
		BigArea.activate();
//		console.log("1 -> 2");
	} else if(sensorReading == 1 && newReading == 3) {
		ThreeD.activate();
		gol.make3D();
//		console.log("1 -> 3");
	} else if(!ThreeD.isActive() && sensorReading == 2 && newReading == 3) {
		ThreeD.activate();
		gol.make3D();
//		console.log("2 -> 2+3");
	} else if(!BigArea.isActive() && sensorReading == 3 && newReading == 2) {
		BigArea.activate();
//		console.log("3 -> 2+3");
	} else if(BigArea.isActive() && ThreeD.isActive() && sensorReading == 3 && newReading == 2) {
		gol.make2D();
		ThreeD.deactivate();
//		console.log("2+3 -> 2");
	} else if(BigArea.isActive() && ThreeD.isActive() && sensorReading == 2 && newReading == 3) {
		BigArea.deactivate();
//		console.log("2+3 -> 3");
	} else if(sensorReading == 2 && newReading == 1) {
		BigArea.deactivate();
//		console.log("2 -> 1");
	} else if(sensorReading == 3 && newReading == 1) {
		gol.make2D();
		ThreeD.deactivate();
//		console.log("3 -> 1");
	}
	gol.step();
	sensorReading = newReading;
}
setInterval(environmentRead, 2*1000);
*/


//-------------- EXECUTION
/*
createBoard();
init([[0,0], [0,1], [1,0], [1,1], [1,2]]);
step();
//step();
//step();
//step();
for(var i=0; i<threeDContexts.length; i++) {
	if(i % 2 == 0) {
		threeDContexts[i].activate();
//		console.log(threeDContexts[i].name());
//		console.log(cells[i]);
		cells[i].make3D();
	}
}
step();

for(var i=0; i<threeDContexts.length; i++) {
	if(threeDContexts[i].isActive()) {
		threeDContexts[i].deactivate();
	}
}
step();
/*/
//---------BigArea
//*
createBoard();
init([[1,0], [1,1], [1,3], [1,4], [2,0], [2,1], [2,3], [2,4], [3,2], [4, 2]]);
for(var i=0; i<bigAreaContexts.length; i++) {
	bigAreaContexts[i].activate();
}
step();
for(var i=0; i<bigAreaContexts.length; i++) {
	bigAreaContexts[i].deactivate();
}
step();
//*/