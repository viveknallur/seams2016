from __future__ import division
from collections import Counter
from collections import defaultdict
import math
import sys
from statistics import median

TOTAL_POPULATION = 501

def calc_ginisimpson(specie_counter):
    lamb_da = 0 # lambda is a reserved word in python
    for k,v in specie_counter.items():
        lamb_da = lamb_da + math.pow((v/TOTAL_POPULATION),2)
    #print("Lambda is: %f"%(lamb_da))
    gini_simpson = 1 - lamb_da
    #print("Gini-Simpson index is: %f"%(gini_simpson))
    return gini_simpson

def calc_shannon(specie_counter):
    H = 0
    num_species = len(specie_counter)
    for k,v in specie_counter.items():
        #print("v = %d, total = %d"%(v, TOTAL_POPULATION))
        prop = v/TOTAL_POPULATION
        #print("Prop = %f"%(prop))
        H = H + (prop * math.log10(prop))
    H = -H
    #print("Shannon is: %f"%(H))
    #print("Num species is: %d"%(num_species))
    
    return [H, num_species]

def process_specie_line (specie_line):
    ## process and return a counter object
    specie_line = specie_line[1:] # remove the starting '['
    specie_line = specie_line[:-2] # remove the ending ']' and newline
    species_arr = specie_line.split()
    specie_counter = Counter(species_arr)
    print("species:")
    for i in specie_counter.items():
        print (i)
    return specie_counter


def get_rows(filename):
    per_cycle_shannon_list = defaultdict(list) # list contains ints
    per_cycle_gini_list = defaultdict(list) # list contains ints
    per_cycle_specie_counter = defaultdict(list) # list contains counter objects
    per_cycle_unique_specie = defaultdict(list) # list contains counter objects
    previous_cloning_cycle = 21
    with open(filename, "r") as speciefile:
        for line in speciefile:
                try:
                    current_cloning_cycle = int(line)
                except ValueError as e:
                    #print("Was actually specie line")
                    specie_line = line
                    current_cloning_cycle = previous_cloning_cycle
                    print("current cloning_cycle: %d"%(current_cloning_cycle))
                    #print("specie line: %s"%(line[0:15]))
                    current_specie_counter = process_specie_line(specie_line)
                    per_cycle_specie_counter[current_cloning_cycle].append(current_specie_counter)
                    current_specie_shannon, curr_specie_count = calc_shannon(current_specie_counter)
                    #current_specie_gini = calc_ginisimpson(current_specie_counter)
                    per_cycle_shannon_list[current_cloning_cycle].append(current_specie_shannon)
                    per_cycle_unique_specie[current_cloning_cycle].append(curr_specie_count)
                    #per_cycle_gini_list[current_cloning_cycle].append(current_specie_gini)
                else:
                    #print("consecutive cloning cycle lines: %d, %d"%(previous_cloning_cycle, current_cloning_cycle))
                    previous_cloning_cycle = current_cloning_cycle
                    continue

    for k,v in per_cycle_shannon_list.items():
        print("Cloning cycle: %d"%(k))
        print("Length of shannon list: %d"%(len(v)))
        print("Average shannon diversity: %f"%(sum(v)/len(v)))
        print("Average balance: %f"%((sum(v)/len(v))/math.log10(TOTAL_POPULATION)))
#
#    for k,v in per_cycle_unique_specie.items():
#        print("Cloning cycle: %d"%(k))
#        print("Max unique species: %f"%(max(v)))
#        print("Min. unique species: %f"%(min(v)))
#        print("Avg. unique species: %f"%(sum(v)/len(v)))
#        print("Median unique species: %f"%(median(v)))
#


#    for k,v in per_cycle_gini_list.items():
#        print("Cloning cycle: %d"%(k))
#        print("Length of gini list: %d"%(len(v)))
#        print("Average gini diversity: %f"%(sum(v)/len(v)))

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("I need a data file")
        exit (1)
    else:
        filename = sys.argv[1]
        filepath = \
    '/home/vivek/bitbucket/seams2016/code/CP/'
        full_filename = ''.join([filepath, filename])
        population = get_rows(full_filename)
