
% !TEX root = adaptation-and-clonal-plasticity.tex


\section{Plastic Adaptation Case Study}
\label{sec:adaptation}

This section showcases the utility and feasibility of a plasticity model to realize decentralized 
adaptation, by implementing an adaptive version of Conway's \ac{GOL}. 
We use \ac{GOL}, as its dynamics are readily known, and the application can be easily decomposed into multiple individuals, each reacting to adaptation scenarios from the environment.
The purpose of this case study is to show how individuals in a system can adapt their behavior with respect to the their own perception of the surrounding execution environment, \ie adaptation is not uniform for all individuals, but rather is heterogeneous. In order to adapt each individual's behavior, we follow a clonal plasticity approach, where individuals adapt immediately after they have sensed a change in their surrounding execution environment. 

The initial setting for our case study is that of the regular \ac{GOL}, \ie a two dimensional grid of cells, each cell being in one of two possible states, alive or dead. The game progresses by changing the state of each cell with respect to the state of their immediate neighbors. Our version of \ac{GOL} is decentralized, detaching cells from a central grid arrangement. Rather, each cell is a free floating individual that finds its neighbors according to their proximity (given by cells' position). Cells use a 1-radius neighborhood to evaluate game progress (\eg demarcated by the blue square centered at the live cell in the figures). Each individual cell in the game can react to two changes in their surrounding execution environment, leading to different adaptations in its behavior. 
\begin{enumerate*}[label=(\arabic*)]
\item The first environment change, causes cells to use larger neighborhoods to update their state as the game progresses. 
\item The second environment change, causes cells to transcend into a three-dimensional space. 
\end{enumerate*}

%%
\subsection{Implementing Decentralized Adaptations}

The two aforementioned adaptation scenarios for \ac{GOL} are implemented using the adaptation mechanisms proposed in \ac{COP}~\cite{hirschfeld+08cop}, to adhere to a plastic reproduction mechanism. In particular, we use  the Context Traits~\cite{gonzalez+13ctxtraits} language for our implementation. Context Traits enables the behavioral adaptation of a software system in response to input from its surrounding execution environment. \ac{COP} is chosen as an implementation mechanism since it satisfies the five steps of the plastic reproduction process introduced in \fref{sec:plastic_reproduction}. 

\begin{enumerate}[leftmargin=10pt]
  \item Our adaptation scenarios affect the interaction between cells. 
      Therefore, the Plasticity Points for all cell individuals in \ac{GOL} are 
      identified precisely as those points where cells interact with each other 
      (\ie the \scode{countAliveNeighbors()}, \scode{livenessConditions()}, and 
      \scode{step()} functions).
%  In Context Traits such adaptations are defined as stand-alone behavior units, defined as shown below for the large neighborhood adaptation as an example.
%\begin{ctxtraits}
% LargeNeighborhoodCell = Trait({
%   countAliveNeighbors: function() {
%     var count = 0;
%       for(dx=-2; dx<= 2; dx++){
%         for(dy = -2; dy <= 2; dy++) {
%           if(dx == 0 && dy == 0) {}
%           else
%             for(i=0; i<cells.length; i++) 
%               if(this.x+dx == cells[i].x && this.y+dy == cells[i].y && cells[i].state) 
%                 count ++;	
%         }
%       }
%     return count;
%   }});
%\end{ctxtraits}
%The two outermost loops in this adaptation search for cells in a 2-radius neighborhood, while the inner loop, goes over all cells discovered in the environment. If there are alive cells in the neighborhood, then these are counted.

  \item Program entities (\ie object instances) sense their environment and evaluate whether their Plasticity Points (\ie behavior) requires adaptation. If this is the case, then adaptations are enacted immediately.
  In our example, the need for adaptation is signaled to an object instance via a \emph{context} object. Contexts associate object instances with their behavior adaptations, for example, for the large neighborhood adaptation, a new \scode{LargeNeighborhood}~$_{i,j}$ context is created, associating a \scode{cell}~$_{i,j}$ instance to the context and the behavioral adaptation defined previously.
%  , as follows 
%\begin{ctxtraits}
%  var LargeNeighborhood`$_{i,j}$` = new cop.Context({
% 	  name: 'LargeNeighborhood'`$_{i,j}$` 
%    });
%	
%  LargeNeighborhood`$_{i,j}$`.adapt(cell`$_{i,j}$`, LargeNeighborhoodCell);
%\end{ctxtraits}  

  \item In our implementation, individuals do not keep an active record of their 
      last adaptation taken.  Rather, each individual is aware of all its 
      adaptations at all time. However, these previous actions do not influence 
      individuals' behavior, and Environmental Input is gathered from external 
      sensors and self.

  \item When adapting an object instance, in our \ac{COP} model, we always choose a low plasticity 
  strategy. That is, adaptations are always assumed to provide the best possible behavior, and hence 
  provide a positive effect, for the object instance with respect to its surrounding environment.
  
  \item Adaptations have an instantaneous effect on the object instances they modify, \ie immediately 
  after the context signals the adaptation, by calling \scode{activate()} on the context (\eg 
  \scode{LargeNeighborhood.activate()}), the object instance associated with such context will 
  immediately use the behavior adaptation associated with the context.
  
  Similar to the introduction of adaptations by means of context activation, adaptations can be reverted 
  by withdrawing adaptive behavior from instance objects using \scode{deactivate()}, \eg 
  \scode{LargeNeighborhood.deactivate()}. Reverting adaptations also has an immediate effect on 
  object instances, withdrawing the corresponding behavior from the plasticity point. 
%  moment in which the observed behavior of the object instance will be its original behavior, or that of any other adaptations currently sensed in the surrounding environment.  
\end{enumerate}


%%
\subsection{Experimental Set-up}

To show the feasibility of the plasticity adaptation mechanism for decentralized environments, we run 
a  \ac{GOL} consisting of $n^2$ cells (\ie individuals).\footnote{The \ac{GOL} implementation and  experiments simulation are available for download at \url{https://bitbucket.org/viveknallur/seams2016.git}.} 
At any moment in time, any of the individuals can engage in any of the adaptations, if it is so required in the surrounding environment. 

Our \ac{GOL} implementation has three plasticity points, the \scode{step()},\scode{countAliveNeighbors()}, and \scode{livenessConditions()} functions. The first and last functions are adaptations in the \scode{3D} context, while the first two functions are adaptations in the \scode{LargeNeighborhood} context. 

%\fref{fig:adaptations} shows the possible adaptation scenarios for our \ac{GOL} implementation. Starting from the top, the game progresses as stipulated by the original rules, where each cell uses a neighborhood of radius 1 (\eg blue square centered on the alive cells in \fref{fig:adaptations}) to verify the conditions to progress to the next step. From this stage the environment can influence change in two ways, the rules change to include larger neighborhoods (moving to the game on the lefthand side of \fref{fig:adaptations}), or the rules change to work on a three-dimensional space (moving to the right-hand side of \fref{fig:adaptations}). 
Note here that, no matter which of the adaptations is taken, this is not a uniform change for all individuals, but only those cells that sensed the change in the surrounding environment adapt. For example, in \fref{fig:adaptations}, in the first case only the leftmost live cell adapted to take into account large neighborhoods in its game rules, while, in the second case, five cells adapted to a three-dimensional space. 
Cells that have adapted, have two choices for a new adaptation, they can either revert the adaptation taken, or they can incorporate the other adaptation, in response to their environment. If the environment is sensed to revert the adaptation, then the game will revert to its original state. In the case of cells that adapted to large neighborhoods, if they now adapt two three-dimensional spaces, then the game will behave as in the bottom of \fref{fig:adaptations}. In the case of cells that adapted to a three-dimensional space, if they now adapt to large neighborhoods, then they will also behave as in the bottom of \fref{fig:adaptations} \ie a three-dimensional space with neighborhoods of radius 2. From such state, either adaptation can be reverted, going back to the adaptation signaled by the environment. For example, if \scode{LargeNeighborhoods} is reverted, the behavior will be that of the three-dimensional space.

\begin{figure}[hptb]
  \centering
  \includegraphics[width=\columnwidth]{figures/adaptations}
  \caption{Adaptation scenarios for \ac{GOL}}
  \label{fig:adaptations}
\end{figure}

%We executed \ac{GOL} in two different settings to ensure its usability, and the 
%feasibility of our approach. The first setting consisted of controlled 
%adaptations of the game. That is, we implemented two fixed scenarios in which 
%contexts will be activated and deactivated deliberately to verify that the game 
%behaved correctly for each adaptation scenario as well as a the combination of 
%both. In this setting, at every step the game progressed according to its rules 
%(\ie regular \ac{GOL} rules and the adapted ones), and when reverting all cells 
%to the original setting, the game continued working normally.
%The second setting consisted on an ``infinite'' run of the game, in which every two seconds each cell would sense the environment and adapt according to the sensed scenario. The cells that sense the adaptation scenario are chosen randomly, but they all adapt to the same scenario, \ie, either large neighborhoods, or three-dimensional space. After this adaptation step, all cells take a step according to the rules currently applying to them, and then the environment is sensed again. 
%
%We see that using the process of clonal reproduction along with plasticity in behaviour allows independent cells to adapt to changes in the rules of \ac{GOL}. Implementing these kinds of adaptations for an individual cell simplifies the adaptation reasoning, since we do not have to account for the entire system as a whole. Performing a MAPE-K based adaptation for such a system, where the environment can signal change of neighborhood size on one part of the system, or three-dimensional space at another part of the system would have been very difficult. The analysis and planning would have had to deal with multiple conflicting signals from the monitoring aspect of the adaptation framework.
\endinput
