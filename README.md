This repository contains all the code required to run and generate results, for both case-studies presented in the paper. The relevant code can be found in the `code` folder, with labels:
* `GoL` for Game of Life, 
* `CP` for Clonal Plasticity, 
* `Evo` for Genetic Algorithm